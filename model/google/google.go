package oauthgoogle

import (
	"encoding/json"
	"io"

	"log"

	"github.com/mattermost/mattermost-server/einterfaces"
	"github.com/mattermost/mattermost-server/model"
)

type GoogleProvider struct {
}

type email struct {
	Value string `json:"value"`
}

type name struct {
	FamilyName string `json:"familyName"`
	GivenName  string `json:"givenName"`
}

type googleUser struct {
	Emails []email `json:"emails"`
	Name   name    `json:"name"`
	Id     string  `json:"id"`
}

func (gu *googleUser) toUser() *model.User {
	log.Print(gu)
	if gu.Emails == nil || len(gu.Emails) < 1 {
		return &model.User{}
	}
	return &model.User{
		FirstName: gu.Name.GivenName,
		LastName:  gu.Name.FamilyName,
		Email:     gu.Emails[0].Value,
		// Is there something better to use as a username than
		// the email address? Probably.
		Username:    model.CleanUsername(gu.Emails[0].Value),
		AuthData:    &gu.Id,
		AuthService: model.USER_AUTH_SERVICE_GOOGLE,
	}
}

func init() {
	provider := &GoogleProvider{}
	einterfaces.RegisterOauthProvider(model.USER_AUTH_SERVICE_GOOGLE, provider)
}

func (m *GoogleProvider) GetIdentifier() string {
	log.Print("google.GetIdentifier()")
	return model.USER_AUTH_SERVICE_GOOGLE
}

func (m *GoogleProvider) GetAuthDataFromJson(data io.Reader) string {
	user := m.GetUserFromJson(data)
	if user.AuthData == nil {
		return ""
	}
	return *user.AuthData
}

func (m *GoogleProvider) GetUserFromJson(data io.Reader) *model.User {
	user := &googleUser{}
	err := json.NewDecoder(data).Decode(user)
	if err != nil {
		log.Print(err)
		return &model.User{}
	}
	return user.toUser()
}
